/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('funChartsPoc')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
