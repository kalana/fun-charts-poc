(function() {
  'use strict';

  angular
    .module('funChartsPoc')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
